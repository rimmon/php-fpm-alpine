# baseimage: alpine
# package: php82 fpm + bash
# build commands:
#   docker build -t php-fpm-alpine . --no-cache ; docker tag php-fpm-alpine rimmon/php-fpm-alpine ; docker push rimmon/php-fpm-alpine
#
FROM php:8.1-fpm-alpine3.18

RUN apk --no-cache --update add \
        autoconf \
        bash \
        alpine-sdk \
        musl-dev \
        gcompat \
        libxml2-dev \
        icu-dev \
        imagemagick-dev \
        libjpeg-turbo-dev \
        libpng-dev \
        libwebp-dev \
        libzip-dev \
        php82 \
        php82-common \
        php82-bcmath \
        php82-dom \
        php82-ctype \
        php82-curl \
        php82-dba \
        php82-exif \
        php82-fileinfo \
        php82-fpm \
        php82-gd \
        php82-iconv \
        php82-intl \
        php82-json \
        php82-ldap \
        php82-mbstring \
        php82-mysqli \
        php82-mysqlnd \
        php82-opcache \
        php82-openssl \
        php82-pecl-imagick \
        php82-pdo \
        php82-phar \
        php82-posix \
        php82-simplexml \
        php82-session \
        php82-sodium \
        php82-xml \
        php82-xmlreader \
        php82-xmlwriter \
        php82-zip \
        tzdata \
    && pecl install redis \
    && docker-php-ext-configure gd --with-jpeg --with-webp \
    && docker-php-ext-enable redis \
    && docker-php-ext-install bcmath exif gd intl mysqli sockets zip \
    && apk del --no-cache alpine-sdk diffutils \
    && rm -rf /var/cache/apk/* \
    && ln -sf /etc/php82 /etc/php \
    && ln -sf /etc/php82/php.ini /usr/local/etc/php/php.ini \
    && ln -f /usr/local/lib/php/extensions/no-debug-non-zts-20210902/redis.so /usr/lib/php82/modules/redis.so \
    && echo "extension=redis.so" > /etc/php82/conf.d/redis.ini \
    && echo "DONE!"


CMD ["php-fpm", "-F"]


